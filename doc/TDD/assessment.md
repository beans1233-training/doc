> Breandán F

> Date: 21/04/2017

# Test Driven Development
Test Driven Development is the process of completing development according to the target (definition of done). The definition of done in the case of Test Driven Development is defined by the success of all tests by the product.

All the tests should be written before any development of the product begins. These tests define the definition of done, and therefore give the target for the product development.

Test Driven Development allows the developers and the client to agree on a _Definition of Done_. A _Definition of Done_ is a an agreement between developer and client as to what the software should be able to offer when it is deemed "_finished_". By producing a list of tests, the client can be assured that unless these tests pass, the software is not completed, and should therefore be developed further before being handed over.

This also allows the developer to have a very clear understanding of the definition of done as the tests are written before the development of the software begins. This means that the developer can work towards passign the tests to complete development, and should therefore not develop any more than is required by the client. Test Driven Development also allows the developer to prove that they have completed everything that the client requested when the process comes to a close as, once agreed upon, the tests themselves act directly as a definition of done; if they all pass successfully, the product is completed.

However, this approach requires that the client thinks of everything they wish the software to do right at the beginning of the _System Development Life Cycle_ (SDLC). This means it is more difficult for the client to make changes later, as development would need to be stopped and more tests put in place before development continues.

<br />
<br />
## Test Driven Development Life Cycle
Test Driven Development essentially consists of two stages which are repeated, in sequence, until the all the requirements for the product are completed.

Step one is to generate a test or set of tests to satisfy each of the product requirements. step two is to create the minimum amount of code required to make each of the tests pass.

These can be further broken down into the following steps:
### 1 - Add Test
The test is added, including all the required checks for the single specified purpose of the test.

### 2 - Fail Test
The test is executed and fails by default as it is testing no code.

### 3 - Develop/Refactor Code
This is the development process of the product. Only the section of the product under the current test is to be developed at this stage, and only enough code to make the test pass, is required.

### 4 - Run Tests
Once the base code and development of the code is completed, the tests are run again. If they fail this step (step 4) counts as step 2. Therefore the next step is to go back to step 3. Steps 2 &amp; 3 are repeated until the test passes.

This process is repeated for each requirement/set of requirements, until all of the product requirements are met. This serves as the minimum viable product.


<br />
<br />
## Testing

<br />
### Unit Testing
Unit Testing is the process of testing a product a single method, operation or process at a time. For instance each `method` of an `object` may be tested individually, treating them as each their own program. If all these tests complete successfully, individually, then the _entire_ class should also pass the tests successfully.

<br />
### Mocking
Mocking is used when the object you are testing relies on another (complex) object to function. To be able to isolate the behaviour of the test subject and remove the possibility of accidentally testing this other dependency, this complex object should be mocked. The dependency is replaced with a temporary object to simulate the behaviour of its real counterpart.

The mock of the complex object should simulate the a real object as closely as possible, if not exactly. All values held within this object should be known to the test ( / tester). This will allow the test itself to focus on the test subject without going on to test potentially unexpected values within the dependency object.

Though the principle of mocking an object is fairly simple, and can be completed by a new object that simply returns the required results as the tests are created (See Test Stubs). However, it is far easier to use a mocking environment to complete this. I have decided to use a library called _EasyMock_, which allows for mocking of objects, and their methods, on the fly.

### Test Stubs [1]
[[1][1]][(www.tutorialspoint.com, 2017)][1]

Unlike mocking, test stubs are methods that only return a static value per usage, in an attempt to simulate the method actually taking place. These values are normally set before each test again, but may be static values, depending on the usage.

An example from this assessment may be the `authenticate` method in the `WebService` class. It may be set to return true every time, from an instance of the object. This would mean that wherever this instance of the `WebService` object is called, the `authenticate` method will always return true, regardless of the values entered.

This may look like:
```java
public boolean authenticate(userID, password){
    return true; // Always Return True
}
```

Stubs can also be setup for specific tests, with small amounts of code to determine the correct response. It will not be accurate to the real method, but will work to simulate the method. For instance:
```java
public boolean authenticate(userID, password){
    if(password.equals("LETMEIN")){
        return true;
    }else{
        return false;
    }
}
```

This method will return true only when the `password` is _"LETMEIN"_. Otherwise it will return false. This can be used as a flag, during testing, to toggle between a true or false response, to test other functionality in the `StockInformation` class.

<br/>
### Dependency Injection [2]
[[2][2]][(Javaranch.com, 2017)][2]

Dependency Injection is a method of providing sepcific dependencies for a class or method to work. A dependency may be a single value variable, an array or an entire object, which will be used to complete the process in the called method. It is passed in as a variable, allowing the dependency to be modified outside the required method.

For instance a method to calculate the wavelength of a wave at a particular frequency in a vacuum might look like this:
```java
public double calculateWaveLength(double speedOfLight, double frequency){
    return (speedOfLight/frequency);
}
```
In this example, `speedOfLight` is the dependency, as it would normally be a static value, however providing it as a variable to the method allows the actual value to be substituted for less/more accurate values, before being injected.

For example:
```java
double speedOfLight = 300_000_000.0; // Approximation of The Speed Of Light
double frequency = 5_000_000.0;
double wavelength = calculateWaveLength(speedOfLight, frequency);
```
OR
```java
double speedOfLight = 299_792_458.0; // More Accurate Value of The Speed Of Light
double frequency = 5_000_000.0;
double wavelength = calculateWaveLength(speedOfLight, frequency);
```

This allows for the required injected dependency value to be changed, depending on the required accuracy in the result. However it also allows inaccurate use of the same dependency and without validate could be dangerous for the operation of the program. For example:
```java
double speedOfLight = 0.0; // Causes a Zero Division Error During Runtime
double frequency = 5_000_000.0;
double wavelength = calculateWaveLength(speedOfLight, frequency);
```

<br />
In this assignment, the dependency injected into the `StockInformation` class is the `WebService` object. The `authenticate` method for this object can be mocked to return the required value for each test. This allows tests to be completed with a mock of the `WebService` class being injected, and the end-user is able to continue using the finished product, by injecting the real `WebService` class.

Dependency Injection is still prone to errors that can be created by passing in incorrect objects. To prevent these errors occuring, loose coupling can be used with injected dependencies. This instead refers to an interface, forcing an object with the correct methods to be supplied, in many variations, without being tied to one specific class.


<br />
<br />
## Stock Information
In this project classes called `StockInformation` and `WebService` have been commissioned for use in a system. There are several elements of this class that are required by the specification.

<br />
### StockInformation
#### Attributes
- **userID**
- **password**
- **symbol**

#### Getters
- Integer **getSymbol(** **)**
- Integer **getUserID(** **)**
- Boolean **isAvailable(** **)**
- String **getCompanyName(** **)**
- Integer **getCurrentPrice(** **)**
- Integer **getNumberOfSharesOutstanding(** **)**
- Integer **getMarketCapitilisationInMillions(** **)**

#### Setters
- void **setAvailable(** Boolean **)**
- void **setCompanyName(** String **)**
- void **setCurrentPrice(** Integer **)**
- void **setNumberOfSharesOutstanding(** Integer **)**
- void **setMarketCapitilisationInMillions(** Integer **)**

#### Other Methods
- StockInformation **constructor(** userID, password, symbol **)**
- String **toString(** **)**

#### Initial Test Plan
| ID | Name                     | Test Subject                 | Description                                                                          |
|:--:|:------------------------:|:----------------------------:|:------------------------------------------------------------------------------------:|
| 1  | constructorAndGetters()  | constructor()                | Expects All Values Passed To The Constructor To Be Returned By The Setters           |
| 2  | gettersAndSetters()      | All Getters and Setters      | All setters will be used to pass a param and getters to check they set properly      |
| 3  | constructorAndToString() | constructor() and toString() | Constructor will set all params and toString will get output in string format        |
| 4  | settersAndToString()     | Setters and ToString()       | Setters will set all _required_ params and toString will get output in string format |



<br />
### WebService
This class is not available for use and will instead need to be mocked during testing. The basic information required to mock an object of this class has been provided.



#### Methods
- Boolean **authenticate(** Integer, String **)**
- String **getStockInfo(** String **)**


<br />
## Test Plan (Revised)
I have expanded each of the above tests into more sepcific tests which can be found in the report document.

[Report][r]

<br />
<br />
## Report
These tests have been planned out in more detail [here][r]. In this [report][r] specifics, such as identification of ranges, exception handling, etc., have ben discussed; why they were chosen, why they are important and how they help to test that the finished product will do as it should. [The report][r] also breaks down each of the above tests into smaller tests to target specifics within the system, such as range handling.

The [report][r] will also include details of dependencies required for each test. These dependencies should also specify any which should be mocked to avoid the testing of other classes than the test subject and provide dependencies which otherwise the tests have no access to.




<br />
<br />
## Bibliography
[[1][1]]: [www.tutorialspoint.com. (2017). Stub. (Accessed 31 May 2017)][1]
[[2][2]]: [Javaranch.com (2017). Dependency Injection and Unit Testing. (Accessed 31 May 2017)][2]


[1]: https://www.tutorialspoint.com/software_testing_dictionary/stub.htm
[2]: http://www.javaranch.com/journal/200709/dependency-injection-unit-testing.html

[r]: report.md
