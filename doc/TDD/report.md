> Breandán F

> Date: 21/04/2017

# Test Driven Development - Report
**NOTE**: This document should not be viewed as a standalone submission! It is part of, and requires, [The Rest of The Assessment Documentation (Essay Included)][a].

In this report specifics, such as identification of ranges, exception handling, etc., have ben discussed; why they were chosen, why they are important and how they help to prove that the _finished_ product is in fact finished. This report also breaks down each of the above tests into smaller tests to target specifics within the system, such as range handling.

This report will also include details of dependencies required for each test. These dependencies should also specify any which should be mocked to avoid the testing of other classes than the test subject and provide dependencies which otherwise the tests have no access to.


<br />
<br />
## Test Details
Referring to the ID column from the [Test Plan Table][a] in the [Testing Document][a] the tests will be grouped by ID and broken down into smaller groups such that `1.1` would be the first test where the parent is `1` and `1.2` would be the second test, and so on.

eg.

    ├──1
    |   ├──1.1
    |   ├──1.2
    |   ├──1.3
    |   └──1..
    |
    ├──2
    │   ├──2.1
    |   ├──2.2
    │   └──2..
    │
    └──...



<br />
### 1
**NOTE**: Title Names are formatted such that:
`<Subtest ID> | <Test Method Name> | <Test Subject Method Name>`


#### 1.1 | getUserID_Valid_Test() | getUserID()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
```
The `getUserID()` method should then return the value 10.
```java
stockInformation.getUserID(); // Returns 10
```

The test should be repeated for the following values:
- userID = 20
- userID = 30
- userID = 40

Returning:
```java
StockInformation stockInformation = new StockInformation(20, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 20
stockInformation = new StockInformation(30, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 30
stockInformation = new StockInformation(40, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 40
```
If successful (as expected) this concludes that the `getUserID()` method returns the value passed in to the constructor.


#### 1.2 | constructor_Boundary_Test_Lower() | constructor()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 0
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(0, "P4$$W0rd", "ABC123"); // Throws Exception: `UserIDLessThan1`
```
An Exception should be thrown to indicate an error with the Entered UserID is Invalid.

The test should be repeated for the following values:
- userID = -1
- userID = 1

Returning:
```java
StockInformation stockInformation = new StockInformation(-1, "P4$$W0rd", "ABC123"); // Throws Exception: `UserIDLessThan1`
stockInformation = new StockInformation(1, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 1
```
If successful (as expected) this concludes that the constructor checks that the entered `userID` is greater than 0.


#### 1.3 | constructor_Boundary_Test_Upper() | constructor()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10000
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10000, "P4$$W0rd", "ABC123");
```
An Exception should be thrown to indicate an error with the Entered UserID is Invalid.

The test should be repeated for the following values:
- userID = 10001
- userID = 9999

Returning:
```java
StockInformation stockInformation = new StockInformation(10001, "P4$$W0rd", "ABC123"); // Throws Exception: `UserIDGreaterThan9999`
stockInformation = new StockInformation(9999, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 9999
```
If successful (as expected) this concludes that the constructor checks that the entered `userID` is less than 10000.


#### 1.4 | getSymbol_Valid_Test() | getSymbol()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
```
The `getSymbol()` method should then return the value "ABC123".
```java
stockInformation.getSymbol(); // Returns "ABC123"
```

The test should be repeated for the following values:
- symbol = "DEF456"
- symbol = "GHI789"
- symbol = "JKL012"

Returning:
```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "DEF456");
stockInformation.getSymbol(); // Returns "DEF456"
stockInformation = new StockInformation(10, "P4$$W0rd", "GHI789");
stockInformation.getSymbol(); // Returns "GHI789"
stockInformation = new StockInformation(10, "P4$$W0rd", "JKL012");
stockInformation.getSymbol(); // Returns "JKL012"
```
If successful (as expected) this concludes that the `getSymbol()` method returns the value passed in to the constructor.


#### 1.5 | constructor_InvalidPassword_Test() | getSymbol()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "NOTP4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10, "NOTP4$$W0rd", "ABC123"); // Throws Exception: `FailedToLogin`
```
An Exception should be thrown to indicate an error with the Entered Password is Invalid.

The test should be repeated for the following values:
- password = "ASDFASDF";
- password = "P4$$W0rd"

Returning:
```java
StockInformation stockInformation = new StockInformation(10, "ASDFASDF", "ABC123"); // Throws Exception: `FailedToLogin`
stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 10
```
If successful (as expected) this concludes that the constructor attempts to login to the WebService with the provided `userID` and `password`.


#### 1.6 | constructor_InvalidUserID_Test() | getSymbol()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 8
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(8, "P4$$W0rd", "ABC123"); // Throws Exception: `FailedToLogin`
```
An Exception should be thrown to indicate an error with the Entered Password is Invalid.

The test should be repeated for the following values:
- userID = 9
- userID = 10

Returning:
```java
StockInformation stockInformation = new StockInformation(9, "P4$$W0rd", "ABC123"); // Throws Exception: `FailedToLogin`
stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 10
```
If successful (as expected) this concludes that the constructor attempts to login to the WebService with the provided `userID` and `password`.



<br />
### 2

#### 2.1 | getters_Valid_Test() | (All Getters and Setters)
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
```
- The `getUserID()` method should then return the value 10.
- The `getSymbol()` method should then return the value "ABC123".
- The `exists()` method should then return the value True.
- The `isAvailable()` method should then return the value True.
- The `getCompanyName()` method should then return the value "Test Company".
- The `getCurrentPrice()` method should then return the value 2000.
- The `getNumberOfSharesOutstanding()` method should then return the value 2000.
- The `getMarketCapitilisationInMillions()` method should then return the value 4000000.

```java
stockInformation.getUserID(); // Returns 10
stockInformation.getSymbol(); // Returns "ABC123"
stockInformation.exists(); // Returns True
stockInformation.isAvailable(); // Returns True
stockInformation.getCompanyName(); // Returns "Test Company"
stockInformation.getCurrentPrice(); // Returns 2000
stockInformation.getNumberOfSharesOutstanding(); // Returns 2000
stockInformation.getMarketCapitilisationInMillions(); // Returns 4000000
```
If successful (as expected) this concludes that the getter methods return the values passed in to, and collected by, the constructor.


#### 2.2 | getters_Invalid_Test() | (All Getters and Setters)
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 50
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(50, "P4$$W0rd", "ABC123");
```
- The `getUserID()` method should then return the value 50.
- The `getSymbol()` method should then return the value "".
- The `exists()` method should then return the value False.
- The `isAvailable()` method should then return the value False.
- The `getCompanyName()` method should then return the value "Not allowed".
- The `getCurrentPrice()` method should then return the value 0.
- The `getNumberOfSharesOutstanding()` method should then return the value 0.
- The `getMarketCapitilisationInMillions()` method should then return the value 0.

```java
stockInformation.getUserID(); // Returns 50
stockInformation.getSymbol(); // Returns ""
stockInformation.exists(); // Returns False
stockInformation.isAvailable(); // Returns False
stockInformation.getCompanyName(); // Returns "Not allowed"
stockInformation.getCurrentPrice(); // Returns 0
stockInformation.getNumberOfSharesOutstanding(); // Returns 0
stockInformation.getMarketCapitilisationInMillions(); // Returns 0
```

The test should be repeated for the following values:
- userID = 10
- symbol = "DEF123"
- symbol = "DEF456"

Returning:
```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "DEF123");
```
- The `getUserID()` method should then return the value 10.
- The `getSymbol()` method should then return the value "".
- The `exists()` method should then return the value False.
- The `isAvailable()` method should then return the value False.
- The `getCompanyName()` method should then return the value "".
- The `getCurrentPrice()` method should then return the value 0.
- The `getNumberOfSharesOutstanding()` method should then return the value 0.
- The `getMarketCapitilisationInMillions()` method should then return the value 0.

```java
stockInformation.getUserID(); // Returns 10
stockInformation.getSymbol(); // Returns ""
stockInformation.exists(); // Returns False
stockInformation.isAvailable(); // Returns False
stockInformation.getCompanyName(); // Returns "Not allowed"
stockInformation.getCurrentPrice(); // Returns 0
stockInformation.getNumberOfSharesOutstanding(); // Returns 0
stockInformation.getMarketCapitilisationInMillions(); // Returns 0
```
```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "DEF456");
```
- The `getUserID()` method should then return the value 10.
- The `getSymbol()` method should then return the value "DEF456".
- The `exists()` method should then return the value True.
- The `isAvailable()` method should then return the value False.
- The `getCompanyName()` method should then return the value "Unavailable Test Company".
- The `getCurrentPrice()` method should then return the value 0.
- The `getNumberOfSharesOutstanding()` method should then return the value 0.
- The `getMarketCapitilisationInMillions()` method should then return the value 0.

```java
stockInformation.getUserID(); // Returns 10
stockInformation.getSymbol(); // Returns "DEF456"
stockInformation.exists(); // Returns True
stockInformation.isAvailable(); // Returns False
stockInformation.getCompanyName(); // Returns "Unavailable Test Company"
stockInformation.getCurrentPrice(); // Returns 0
stockInformation.getNumberOfSharesOutstanding(); // Returns 0
stockInformation.getMarketCapitilisationInMillions(); // Returns 0
```
If successful (as expected) this concludes that the getter methods return the values passed in to the constructor and attempts to authenticate with WebService; reset all other values if not able to authenticate.


<br />
### 3

#### 3.1 | toString_Valid_Test() | toString()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
```
The `getStockInfo()` method should then return the value "ABC123,Test Company,2000,2000" therefore the `toString()` method would return "Test Company [ABC123] 2000".

```java
stockInformation.toString(); // Returns "Test Company [ABC123] 2000"
```
If successful (as expected) this concludes that the to String method returns the information in the correct format.


#### 3.2 | toString_Invalid_Test() | toString()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "XYZ999"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "XYZ999");
```
The `getStockInfo()` method should then return the value "", as the `isAvailable` boolean value should be `False` therefore the `toString()` method would return "Not Found".

```java
stockInformation.toString(); // Returns "Not Found"
```
If successful (as expected) this concludes that the to String method returns the information in the correct format.


<br />
### 4

#### 4.1 | toString_ValidSetters_Test() | toString()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC123"

THEN

- symbol = "DEF456"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");

stockInformation.setSymbol("DEF456");
```
The `getStockInfo()` method should then return the value "DEF456,Company Test,3000,2500" therefore the `toString()` method would return "Company Test [DEF456] 3000".

```java
stockInformation.toString(); // Returns "Company Test [DEF456] 3000"
```
If successful (as expected) this concludes that the to String method returns the information in the correct format.


#### 4.2 | toString_InvalidSetters_Test() | toString()
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC123"

THEN

- symbol = "XYZ999"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");

stockInformation.setSymbol("XYZ999");
```
The `getStockInfo()` method should then return the value "", as the `isAvailable` boolean value should be `False` therefore the `toString()` method would return "Not Found".

```java
stockInformation.toString(); // Returns "Not Found"
```

And in reverse:

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "XYZ999");

stockInformation.setSymbol("ABC123");
```
The `getStockInfo()` method should then return the value "ABC123,Test Company,2000,2000" therefore the `toString()` method would return "Test Company [ABC123] 2000".

```java
stockInformation.toString(); // Returns "Test Company [ABC123] 2000"
```
If successful (as expected) this concludes that the to String method returns the information in the correct format.

## Development
After completing the tests I ran junit to show that all the tests still failed, due to the lack of code in StockInformation, and that all the 'fail' methods had been removed.

![Failed Tests][i1]

I can now begin to satisfy each of these tests, starting with 1.1, by developing the necessary parts of the `StockInformation` class.

### 1.1
To make test 1.1 pass, a constructor, for `StockInformation`, must be implemented, that will take an instance of the `WebService` as an injected dependency, the `userID`, the `password` and the stock `symbol`. The `userID` and `password` should be passed to the `WebService` method `authenticate`. If `authenticate` returns `TRUE`, the `userID` should be set as a global variable to pull back out with a _getter_.

![Test 1.1 Satisfactory Code][i2]
![Test 1.1 Passed][i3]

### 1.2
To make test 1.2 pass, some validation, for the `userID`, must now be included, to check that it is greater than 0. This can be added as an if statement before the authentication check.

![Test 1.2 Satisfactory Code][i4]
![Test 1.2 Passed][i5]

### 1.3
To make test 1.3 pass, some extra validation, for the `userID`, must be added, to check that it is less than 10000. This can also be added as another if statement in the constructor.

![Test 1.3 Satisfactory Code][i6]
![Test 1.3 Passed][i7]

### 1.4
To make test 1.4 pass, I now need to include setting a global variable for the `symbol` to pull back out with a _getter_ later.

![Test 1.4 Satisfactory Code][i8]
![Test 1.4 Passed][i9]

### 1.5
Test 1.5 requires verification of the authentication process. For this reason, the response from `WebService` is mocked before dependency injection, and now returns false when the _incorrect_ password is entered. For this reason another exception must be thrown when authentication fails. The `Company Name` is also to be set to `Not Allowed` if authentication fails, for future reference to the same `StockInformation` object. This can simply be added as an `else` to the end of the if statement.

![Test 1.5 Satisfactory Code][i10]
![Test 1.5 Passed][i11]

### 1.6
Test 1.6 is also satisfied by the same code as 1.5.

![Test 1.6 Satisfactory Code][i10]
![Test 1.6 Passed][i12]

### 2.1
Test 2.1 simply requires all the _getters_ and _setters_ to be implemented. Once implemented, the `getStockInfo` method should be used to populate the variables. However, `getStockInfo` returns a string, which means some string manipulation will be required in a method, I decided to call `setStockInfo`, will be required to parse the comma seperated string into each of the required values. I have decided to call this method whenever the symbol is set, allowing the change of symbol to automatically change the corresponding values. I have also added in regex to the setter to confirm that the provided symbol is in the correct format before using it to request information from the `WebService`.

![Test 2.1 Symbol Setter Code][i13]
![Test 2.1 Comma Serparated String Parse Code][i14]
![Test 2.1 Passed][i15]

### 2.2
Test 2.2 is also satisfied by the same code as 2.1.

![Test 2.2 Symbol Setter Code][i13]
![Test 2.2 Comma Separated String Parse Code][i14]
![Test 2.2 Passed][i16]

### 3.1
Test 3.1 requires an override of the `java.lang.object.toString()` method. It should return the name of the company, the symbol and the current price. This can be done by simply building a string including the global variables, from the _getters_. If the available boolean value has not been set, the `toString()` method will return "Not Found".

![Test 3.1 Satisfactory Code][i17]
![Test 3.2 Passed][i18]

### 3.2
Test 3.2 is satisfied by the combination of the previous tests.

![Test 3.2 Passed][i19]

### 4.1
Test 4.1 is satisfied by the combination of the previous tests.

![Test 4.1 Passed][i20]

### 4.2
Test 4.2 is satisfied by the combination of the previous tests.

![Test 4.2 Passed][i21]

### Other Tests That I Missed
Now that I have finished satisfying the tests I have realised I have failed to test for two possible circumstances. However, due to the agile approach I have decided to take I can now add these tests.

The first test tests the length of the password before even attempting to authenticate (minimum of 8 characters long).

The second is a test of the symbol. Though I have implemented the code to check that it only consists of letters (upper and lowercase) and numbers before I try to use it to get information, I don't actually test with any values, for the symbol, that contain anything other than letters (upp and lowercase) or numbers.

### E1 - Test
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W"
- symbol = "ABC123"

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W", "ABC123"); // Throws Exception: `PasswordTooShortException`
```
The constructor should throw a new `PasswordTooShortException`.

The test should be repeated for the following values:
- password = "P4$$W0"
- password = "P4$$W0r"
- password = "P4$$W0rd"

Returning:
```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0", "ABC123"); // Throws Exception: `PasswordTooShortException`
stockInformation = new StockInformation(10, "P4$$W0r", "ABC123"); // Throws Exception: `PasswordTooShortException`
stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 10
```
If successful (as expected) this concludes that the `constructor` does check the length of the password before attempting to authenticate.

I will now also have to amend all the try/catch blocks in all my tests to handle the new `PasswordTooShortException`.

### E1 - Development
This test can be satisfied by adding another if statement to the constructor, and throwing a new `PasswordTooShortException` if the password length is less than 8 characters long.

![Test E1 Satisfactory Code][i22]
![Test E1 Passed][i23]

### E2 - Test
A new instance of `StockInformation` class is generated passing the following values into the constructor:

- userID = 10
- password = "P4$$W0rd"
- symbol = "ABC!£$";

```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC!£$"); // Throws Exception: `SymbolInvalidException`
```
The constructor should throw a new `SymbolInvalidException`.

The test should be repeated for the following values:
- symbol = "ABC1£$"
- symbol = "ABC12$"
- symbol = "ABC123"

Returning:
```java
StockInformation stockInformation = new StockInformation(10, "P4$$W0rd", "ABC1£$"); // Throws Exception: `SymbolInvalidException`
stockInformation = new StockInformation(10, "P4$$W0rd", "ABC12$"); // Throws Exception: `SymbolInvalidException`
stockInformation = new StockInformation(10, "P4$$W0rd", "ABC123");
stockInformation.getUserID(); // Returns 10
```
If successful (as expected) this concludes that the `constructor` does check the length of the password before attempting to authenticate.

I will now also have to amend all the try/catch blocks in all my tests to handle the new `SymbolInvalidException`.

### E2 - Development
This test can be satisfied by adding an else clause to the if statement in the symbol _setter_, already in place, and throwing a new `SymbolInvalidException` if the symbol didn't match the regex for the if statement. This wouldn't normally compile however I have added a _throws_ clause to the method, to throw the exception back to where it is called (the constructor) which will in turn throw it to wherever the class is instantiated.

![Test E2 Satisfactory Code][i24]
![Test E2 Passed][i25]

<br />
<br />
## Test Dependencies

### 1
#### 1.1
- 10 is a Valid UserID
- 20 is a Valid UserID
- 30 is a Valid UserID
- 40 is a Valid UserID

#### 1.2
- 1 is a Valid UserID

#### 1.3
- 9999 is a Valid UserID

#### 1.5
- 10 is a Valid UserID
- "P4$$W0rd" is a Valid Password
- NOT"P4$$W0rd" is NOT a Valid Password

#### 1.6
- 8 is an Invalid UserID
- 9 is an Invalid UserID
- 10 is an Valid UserID

### 2
#### 2.1
- WebService returns the following attributes:
    - available = True
    - companyName = "Test Company"
    - currentPrice = 2000
    - numberOfSharesOutstanding = 2000

#### 2.2
- WebService returns the following atttributes for Symbol "ABC123" and UserID 50:
    - exists = False
    - available = False
    - companyName = ""
    - currentPrice = 0
    - numberOfSharesOutstanding = 0
    - marketCapitilisationInMillions = 0
- WebService returns the following atttributes for Symbol "DEF123" and UserID 10:
    - exists = False
    - available = False
    - companyName = ""
    - currentPrice = 0
    - numberOfSharesOutstanding = 0
    - marketCapitilisationInMillions = 0
- WebService returns the following atttributes for Symbol "DEF123" and UserID 10:
    - exists = True
    - available = False
    - companyName = "Unavailable Test Company"
    - currentPrice = 0
    - numberOfSharesOutstanding = 0
    - marketCapitilisationInMillions = 0

### 3
#### 3.1
- 10 is a Valid UserID
- ABC123 is a Valid Symbol

#### 3.2
- 10 is a Valid UserID

### 4
#### 4.1
- 10 is a Valid UserID
- ABC123 is a Valid Symbol
- DEF456 is a Valid Symbol

#### 4.2
- 10 is a Valid UserID
- ABC123 is a Valid Symbol
- XYZ999 is an Invalid Symbol


[a]: assessment.md


<!-- Images -->

[i1]: images/report/allFailed.png

<!-- 1.1 -->
[i2]: images/report/test1_1_code.png
[i3]: images/report/test1_1_passed.png
<!-- 1.1 -->

<!-- 1.2 -->
[i4]: images/report/test1_2_code.png
[i5]: images/report/test1_2_passed.png
<!-- 1.2 -->

<!-- 1.3 -->
[i6]: images/report/test1_3_code.png
[i7]: images/report/test1_3_passed.png
<!-- 1.3 -->

<!-- 1.4 -->
[i8]: images/report/test1_4_code.png
[i9]: images/report/test1_4_passed.png
<!-- 1.4 -->

<!-- 1.5 -->
[i10]: images/report/test1_5_code.png
[i11]: images/report/test1_5_passed.png
<!-- 1.5 -->

<!-- 1.6 -->
[i12]: images/report/test1_6_passed.png
<!-- 1.6 -->

<!-- 2.1 -->
[i13]: images/report/test2_1_code1.png
[i14]: images/report/test2_1_code2.png
[i15]: images/report/test2_1_passed.png
<!-- 2.1 -->

<!-- 2.2 -->
[i16]: images/report/test2_2_passed.png
<!-- 2.2 -->

<!-- 3.1 -->
[i17]: images/report/test3_1_code.png
[i18]: images/report/test3_1_passed.png
<!-- 3.1 -->

<!-- 3.2 -->
[i19]: images/report/test3_2_passed.png
<!-- 3.2 -->

<!-- 4.1 -->
[i20]: images/report/test4_1_passed.png
<!-- 4.1 -->

<!-- 4.2 -->
[i21]: images/report/test4_2_passed.png
<!-- 4.2 -->

<!-- Images -->
