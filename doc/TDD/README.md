# Test Drive Development Readme
The markdown files for this assessment have been provided, as well as a PDF export of these files. However, the PDF version of the markdown files are not always accurate of the original markdown. For this reason it is best to view the markdowns as intended.

To view a markdown, either find a chrome extension or another tool to view the markdown, or simply visit the [Git repository](https://gitlab.com/beans1233-training/doc), and navigate to the relevant markdown file, to have it automatically rendered in HTML and CSS.
